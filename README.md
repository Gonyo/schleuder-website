Schleuder website
=================

Source files to be built by jekyll.

Powers <https://schleuder.nadir.org/>.

To suggest changes or report bugs please use the issue tracker or clone, change and file a merge-request.
